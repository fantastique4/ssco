from django.shortcuts import render
from django.template.loader import get_template
from app_mainAndLogin.models import Perusahaan 

response={}
def index(request):
	response['user'] = Perusahaan.objects.all().first()
	response['base'] = get_template('base.html')
	return render(request, 'profile.html', response)