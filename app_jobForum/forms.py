from django import forms

class Add_New_Forum_Form(forms.Form):
    forum_attrs = {
        'class': 'form-control',
        'placeholder':'Please fill out your new secret villain job',
        'rows':10,
        'cols':100
    }

    forum = forms.CharField(label='',widget=forms.Textarea(attrs=forum_attrs), required=True)

class Add_New_Comment(forms.Form):
    comment_attrs = {
        'class': 'form-control',
        'placeholder':'Please fill out your new secret villain job',
        'rows':10,
        'cols':100
    }

    name_attrs = {
        'placeholder':'Please fill out your name',
        'rows':1,
        'cols':100
    }

    name =  forms.CharField(label='',widget=forms.Textarea(attrs=name_attrs), required=True)
    comment = forms.CharField(label='',widget=forms.Textarea(attrs=comment_attrs), required=True)