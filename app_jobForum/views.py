from django.shortcuts import render
from django.template.loader import get_template
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .forms import Add_New_Forum_Form, Add_New_Comment
from .models import Forum, Comments

response={}
def index(request):
    response['base'] = get_template('base.html')
    response['forum_form'] = Add_New_Forum_Form
    response['all_forum'] = Forum.objects.all()
    html = 'job_forum.html'
    return render(request, html, response)

def add_forum(request):
    form = Add_New_Forum_Form(request.POST or None)

    if (request.method == 'POST' and form.is_valid()):
        response['new_forum'] = request.POST['forum']
        forum = Forum(forum=response['new_forum'])
        forum.save() 
        return HttpResponseRedirect('/job-forum/')
    else:
    	return HttpResponseRedirect('/job-forum/')

def paginate_page(page, data_list):
    paginator = Paginator(data_list, 5) # pragma: no cover

    try: # pragma: no cover
        data = paginator.page(page)
    except PageNotAnInteger: # pragma: no cover
        data = paginator.page(1)
    except EmptyPage: # pragma: no cover
        data = paginator.page(paginator.num_pages)

    index = data.number - 1 # pragma: no cover

    max_index = len(paginator.page_range) # pragma: no cover

    start_index = index if index >= 5 else 0 # pragma: no cover
    end_index = 5 if index < max_index - 5 else max_index # pragma: no cover

    page_range = list(paginator.page_range)[start_index:end_index] # pragma: no cover
    paginate_data = {'data':data, 'page_range':page_range} # pragma: no cover
    return paginate_data

def get_comment_by_forum_id(id):
    return Comments.objects.filter(forum_id_id=id)

def get_forum_by_forum_id(id):
    return Forum.objects.get(pk=id)

def comments(request, id):
    html = 'comments.html'
    response['base'] = get_template('base.html')
    response['id'] = id
        
    response['forum'] = get_forum_by_forum_id(id)

    response['all_comments'] = get_comment_by_forum_id(id)

    response['comment_form'] = Add_New_Comment
    # print(id)
    # print (Comments.objects.all())
    # print(response['all_comments'])

    return render(request, html, response)

    # obj_forum = get_forum_by_forum_id(id)
    # response['all_comment'] = get_comment_by_forum_id(id)

    # return render(request, html, response)

def add_comment(request, id):
    form = Add_New_Comment(request.POST or None)

    if (request.method == 'POST' and form.is_valid()):
        response['new_comment'] = request.POST['comment']
        response['new_name'] = request.POST['name']
        comment = Comments(comment_content=response['new_comment'], forum_id_id=id, name=response['new_name'])
        comment.save() 
        return HttpResponseRedirect('/job-forum/comments/' + id)
    else:
    	return HttpResponseRedirect('/job-forum/comments/' + id)