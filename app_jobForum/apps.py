from django.apps import AppConfig


class AppJobforumConfig(AppConfig):
    name = 'app_jobForum'
