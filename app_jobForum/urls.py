from django.conf.urls import url
from .views import index, add_forum, comments, add_comment

urlpatterns = [
	url(r'^$', index, name='index'),
	url(r'^add-forum/$', add_forum, name='add_forum'),
	url(r'^add-comment/(?P<id>.*)/$', add_comment, name='add_comment'),
	url(r'^comments/(?P<id>.*)/$', comments, name='comments'),
]