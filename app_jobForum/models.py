from django.db import models

class Forum(models.Model):
	forum = models.TextField()
	created_date = models.DateTimeField(auto_now_add=True)
   
	class Meta:
		ordering = ['-created_date']

class Comments(models.Model):
	forum_id = models.ForeignKey(Forum)
	name = models.TextField()
	comment_content = models.TextField()
	created_at = models.DateTimeField(auto_now_add=True)
	# updated_at = models.DateTimeField(auto_now=True)