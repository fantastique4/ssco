from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import *
from .models import Forum

class jobForumUnitTest(TestCase):
    def test_jobForum_url_exist(self):
        response = Client().get('/job-forum/')
        self.assertEqual(response.status_code, 200)

    def test_jobForum_using_index_func(self):
        found = resolve('/job-forum/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_forum(self):
        # Creating a new activity
        new_forum = Forum.objects.create(forum='Let us destroy human and conquer the world, YEAY')

        # Retrieving all available activity
        counting_all_forum = Forum.objects.all().count()
        self.assertEqual(counting_all_forum, 1)