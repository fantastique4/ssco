from django.shortcuts import render
from django.template.loader import get_template

response={}
def index(request):
    response['base'] = get_template('base.html')
    return render(request, 'respond.html', response)