from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import *

class respondUnitTest(TestCase):
    def test_respond_url_exist(self):
        response = Client().get('/respond/')
        self.assertEqual(response.status_code, 200)

    def test_respond_using_index_func(self):
        found = resolve('/respond/')
        self.assertEqual(found.func, index)