from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import *

class mainUnitTest(TestCase):
    def test_main_url_exist(self):
        response = Client().get('/main/')
        self.assertEqual(response.status_code, 200)

    def test_jobForum_using_index_func(self):
        found = resolve('/main/')
        self.assertEqual(found.func, index)