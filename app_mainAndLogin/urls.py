from django.conf.urls import url
from .views import index, add_perusahaan
#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add_perusahaan/', add_perusahaan, name='add_perusahaan'),
]