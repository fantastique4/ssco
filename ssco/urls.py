"""ssco URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic import RedirectView
import app_jobForum.urls as jobForum
import app_mainAndLogin.urls as mainAndLogin
import app_profile.urls as profile
import app_respondToMain.urls as respond

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^job-forum/', include(jobForum,namespace='jobForum')),
	url(r'^main/', include(mainAndLogin,namespace='mainAndLogin')),
	url(r'^profile/', include(profile,namespace='profile')),
	url(r'^respond/', include(respond,namespace='respond')),
	url(r'^$', RedirectView.as_view(url='/main/',permanent='true'), name='index'),
]
