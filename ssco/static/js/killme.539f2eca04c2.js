
// Setup an event listener to make an API call once auth is complete
    function onLinkedInLoad() {
        console.log("masuk login")
        IN.Event.on(IN, "auth", getCompanyData);
        //redirect ke profile
    }

    // Handle the successful return from the API call
    function onSuccess(data) {
        console.log(data);
    }

    // Handle an error response from the API call
    function onError(error) {
        console.log(error);
    }

    // Use the API call wrapper to request the member's basic profile data
    function getProfileData() {
        // IN.API.Raw("/people/~").result(onSuccess).error(onError);
        IN.API.Profile("me").fields("id", "first-name", "last-name", "headline", "location", "picture-url", "public-profile-url", "email-address").result(displayProfileData).error(onError);

    }
    function displayProfileData(data){
        console.log(data)
        var user = data.values[0];
        sessionStorage.setItem("picture", user.pictureUrl);
        const name = user.firstName + user.lastName;
        sessionStorage.setItem("name", name);
        
    }
    function getCompanyData() {
            var cpnyID = 13600857;
        IN.API.Raw("/companies/" + cpnyID + ":(id,name,ticker,description,company-type,founded-year,locations:(address),website-url,square-logo-url,specialties,industries,employee-count-range,logo-url)?format=json").method("GET").result(displayCompanyData).error(onError);
    }
    function displayCompanyData(data){
        console.log("masuk company data");
        console.log(data);
        console.log("ini session sekarang " + sessionStorage.getItem("logged_in"));
        sessionStorage.setItem("name", data.name);
        sessionStorage.setItem("desc", data.description);
        sessionStorage.setItem("tipe", data.companyType.name);
        sessionStorage.setItem("weburl", data.websiteUrl);
        sessionStorage.setItem("specialties", data.specialties.values);
        sessionStorage.setItem("logourl", data.logoUrl);
        buatbaru(data);
        if(sessionStorage.getItem("logged_in") == undefined){
            window.location.href = "/profile/";    
            sessionStorage.setItem("logged_in", 1);
        };
        if(sessionStorage.getItem("logged_in") == 0){
            window.location.href = "/profile/";    
            sessionStorage.setItem("logged_in", 1);
        };
    }

    var buatbaru = function(data){
        console.log("buat baru")
        $.ajax({
            method: "POST",
            url: '{% url "mainAndLogin:add_perusahaan" %}',
            data: {data},
            success: function(data){
                console.log(data);
                id = data['id'];
            }
        })
    }
    
    // Handle an error response from the API call
    function onError(error) {
        console.log(error);
    }

    // Destroy the session of linkedin
    function logout(){
        IN.User.logout(removeProfileData);
        sessionStorage.setItem("logged_in", 0);
    }

    // Remove profile data from page
    function removeProfileData(){
        document.getElementById('profileData').remove();
        location.reload();
    }
