function onLinkedInLoad() {
        IN.Event.on(IN, "auth", getProfileData);
    }
    
// Use the API call wrapper to request the member's profile data
function getProfileData() {
    IN.API.Profile("me").fields("id", "first-name", "last-name", "headline", "location", "picture-url", "public-profile-url", "email-address").result(displayProfileData).error(onError);
}

// Handle the successful return from the API call
function displayProfileData(data){
    console.log(data)
    var user = data.values[0];

    document.getElementById("picture").innerHTML = '<img src="'+user.pictureUrl+'" />';
    document.getElementById("name").innerHTML = user.firstName+' '+user.lastName;
    document.getElementById("intro").innerHTML = user.headline;
    document.getElementById("email").innerHTML = user.emailAddress;
    document.getElementById("location").innerHTML = user.location.name;
    document.getElementById("link").innerHTML = '<a href="'+user.publicProfileUrl+'" target="_blank">Visit profile</a>';
    document.getElementById('profileData').style.display = 'block';

    IN.API.Raw('companies?format=json&is-company-admin=true').method('GET').result(getListCompanies);
}

getListCompanies = (data)=>{
    console.log(data)
    console.log(data.values.length)
    console.log(typeof(data.values))
    comp = data.values
    for (i=0;i<data.values.length;i++){
        console.log(comp)
        console.log(comp[0].id)
        $('#company-list').append(
            '<a onClick="openProfile('+data.values[i].id+')" class="list-group-item">'+data.values[i].name+'</a>'
        )
    }
}

openProfile= (id)=>{
    IN.API.Raw('companies/'+id+':(id,name,email-domains,company-type,industries,twitter-id,website-url,logo-url,employee-count-range,specialties,locations,description,founded-year,num-followers)?format=json').method('GET').result(postCompanyProfileData)
    }

postCompanyProfileData=(data)=>{
    console.log(data)
    $.ajax({
        method: "POST",
        url: '/login/add_company/',
        data: {
            'data': data
        },
        beforeSend: function(xhr) {
            xhr.setRequestHeader("X-CSRFToken", "{{ csrf_token }}");
        },
        dataType: 'json',
        success: function (){
            console.log("sukses")
            window.open('profil/'+id+'/', '_self')            
        },
        error: function (error){
            console.log(error)
        }

    })
}
// Handle an error response from the API call
function onError(error) {
    console.log(error);
}

// Destroy the session of linkedin
function logout(){
    IN.User.logout(removeProfileData);
}

// Remove profile data from page
function removeProfileData(){
    document.getElementById('profileData').remove();
}